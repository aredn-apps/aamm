AREDN Alert Message Manager
===========================

AAMM allows for easy management of AREDN[^1] alert messages. From a single
console AAMs can be created for all, individual nodes or group AAMs.

It is recommended that AAMM be installed on a standard AREDN node. Future
versions will have documentation on the requirements for installing on
non-node installations. One of the complications of non-node installations
is having access to a list of the current nodes.

Alert Representation
--------------------

Alerts are standard text files that are accessed usually under `/aam` on
a centralized web server. There is a standard filename of `all.txt` that
all nodes will read. In addition each node will look for a text file named
after its node name.

For further information about AAMs, please see [AREDN docs on AAMs](https://docs.arednmesh.org/en/latest/arednGettingStarted/advanced_config.html#aredn-trade-alert-messages).

## AAM Manager

Web application to manage AREDN alert messages. Allows the management of
AAMs from web browser instead of having to log into a node and edit files.


[^1]: AREDN&reg; is a registered trademark of Amateur Radio Emergency Data
      Network, Inc. The official AREDN repositories can be found at
      https://github.com/aredn.
