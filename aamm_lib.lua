--
require("socket")

local json = require("json")

function debug(msg)
    if config.debug then
        print(msg)
    end
end

function dumpTable( t )

    local dumpTable_cache = {}
    local dumpString = ""

    local function sub_dumpTable( t, indent )
        local dumpString = ""

        if ( dumpTable_cache[tostring(t)] ) then
            dumpString = dumpString .. indent .. "*" .. tostring(t)
        else
            dumpTable_cache[tostring(t)] = true
            if ( type( t ) == "table" ) then
                for pos,val in pairs( t ) do
                    if ( type(val) == "table" ) then
                        dumpString = dumpString .. indent .. "[" .. pos .. "] => " .. tostring( t ).. " {\n"
                        dumpString = dumpString .. sub_dumpTable( val, indent .. string.rep( " ", string.len(pos)+8 ) ) .. "\n"
                        dumpString = dumpString .. indent .. string.rep( " ", string.len(pos)+6 ) .. "}\n"
                    elseif ( type(val) == "string" ) then
                        dumpString = dumpString .. indent .. "[" .. pos .. '] => "' .. val .. '"' .. "\n"
                    else
                        dumpString = dumpString .. indent .. "[" .. pos .. "] => " .. tostring(val) .. "\n"
                    end
                end
            else
                dumpString = dumpString .. indent..tostring(t) .. "\n"
            end
        end
        return dumpString
    end

    if ( type(t) == "table" ) then
        dumpString = dumpString .. tostring(t) .. " {\n"
        dumpString = dumpString .. sub_dumpTable( t, "  " ) .. "\n"
        dumpString = dumpString .. "}\n"
    else
        dumpString = dumpString .. sub_dumpTable( t, "  " ) .. "\n|"
    end

    return dumpString
end

function error(msg)
    text_page(msg)
end

function json_page(data)
    print("Content-type: application/json\n\n")

    print(json.encode(data))
end

function text_page(data)
    print("Content-type: text/plain\n\n")

    print(data)
end

function api_result(code, data)
    data = data or {}

    json_page({
        result = code,
        data = data
    })
end

function aam_file_path(alert_file)
    return Config.aam_dir .. "/" .. string.lower(alert_file)
end

local lock_fd = false
function get_lock(lock_file)
    while (not os.execute("mkdir " .. lock_file .. " >/dev/null 2>&1")) do
        socket.select(nil, nil, 0.2)
    end
    lock_fd = true
end

function release_lock(lock_file)
    if (lock_fd) then
        os.execute("rmdir " .. lock_file)
        lock_fd = false
    end
end

function string.unquote(text)
    local escapes = { ["#32;"] = " ",
                      ["#43;"] = "+", }

    for match, repl in pairs(escapes) do
        text = string.gsub(text, match, repl)
    end

    return text
end

function getenv()
    local osEnv = {}

    for line in io.popen("set"):lines() do
        envName = line:match("^[^=]+")
        osEnv[envName] = os.getenv(envName)
    end

    return osEnv
end

function filelist(dir)
    local files = {}
    for file in io.popen("ls -1 " .. dir .. "/*.txt"):lines() do
        file = file:gsub(Config.aam_dir .. "/", "")
        table.insert(files, file)
    end
    return files
end

function file_exists(path)
    local f = io.open(path, "r")
    return f ~= nil and io.close(f)
end

function file_size(path)
    local f = io.open(path, "r")
    local size = f:seek("end")
    f:close()
    return size
end

function file_mtime(path)
    local proc = io.popen("ls --full-time " .. path, "r")
    local file_info = proc:read("*all")
    proc:close()
    -- -rw-r--r--    1 root     root             0 2023-09-29 00:32:22 -0400 foo

    local yr, mo, da, hr, mi, se, tz = file_info:match("[-rwx]+%s+%d+%s%w+%s+%w+%s+%d+%s(%d+)-(%d+)-(%d+)%s(%d+):(%d+):(%d+)%s([-+%d]+)%s")
    local offset = (tz / 100) * 3600
    return os.time({year=yr, month=mo, day=da, hour=hr, min=mi, sec=se}) - offset
end

