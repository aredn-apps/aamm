Installation
============

Currently the OpenWRT packages are generated automatically as changes to
AAM Manager are committed. These packages can be installed on the AREDN
firmware directly in the Administration setup page. All the current
builds and releases can be found at:

    https://gitlab.com/aredn-apps/aamm/-/packages?type=&orderBy=created_at&sort=desc

All releases use semantic versioning (i.e x.x.x) whereas development builds
all start with the date of the build (i.e. 20030930-... ). It is advisable
to download and use a release build unless there is a specific feature that
is needed yet to be released.

Once downloaded, the package file (.ipk extension) can be installed by
using the Upload Package button on the Administration setup page. Once
installed one should log into the node and configure AAMM. Please see
[configuring AAMM](Configuration.md) for more information. In most cases
only the password and AAM groups need to be updated.

Installation on Linux
---------------------

Currently installing on Linux or some other UNIX variant requires manual
install. The prerequisite for AAMM is to install LUA (v5.4 or greater) on
the Linux host. Once this requirement has been satisfied, the following
procedure will install AAMM:

1. Create a `aam` subdirectory in the HTML root directory (usually `/var/www/html`)
2. Copy the files from the `www` repo directory to this `aam` directory
3. Locate the web server's `cgi-bin` directory (usually `/usr/lib/cgi-bin`)
4. Copy the `aamm*` files from the repo directory to the `cgi-bin` directory
5. Locate the LUA modules directory (usually `/usr/lib/lua`)
6. Recursively copy the files in `lib` repo directory to the modules directory
