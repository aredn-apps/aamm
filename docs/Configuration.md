Configuration
=============

Configuration of AAM Manager is controlled by the `aamm_config.lua` file
that is usually located in the web server's `cgi-bin`. For an AREDN[^1] node
installation this location would be `/www/cgi-bin`. For other
installations, please consult the web server documentation for the
location of the `cgi-bin` directory.

The configuration consists of a standard LUA table object named `Config`.
The following table keys are recognized.

|     Key    |   Default   |   Description                                 |
|------------|-------------|-----------------------------------------------|
| aam_dir    | '/www/aam'  | The directory on the web server to store AAMs |
| api_url    | '/cgi-bin/aamm.lua' | The URL path to the `aamm.lua` script   |
| password   | 'change_me' | The administrator password used to manage AAMs |
| debug      | false       | Used to control debugging messages in the browser console |
| lock_file  | '/www/aam/lock' | Lock file location to prevent simultaneous saves of the same AAM |
| alert_groups | { }       | List of AAM group names for generating group AAMs |

String values in the configuration file need to be quoted (either single or
double quotes) in order for the LUA scripting language to properly read the
values. `false` and `true` values are not quoted (only `debug` setting uses
these values) as they are valid LUA keywords.

The list of AAM group names is a list of comma separated quoted (single or double quotes) words enclosed in curly braces. To avoid confusion, here is
an example:

```
      alert_groups = { 'wx', 'ares', 'skywarn' }
```

The AAM group names can contain upper case characters to make them more
readable and consumed by humans. AAMM will always convert the group name
to lower case when creating the alert file so that the AREDN nodes locate
the correct alert file.

[^1]: AREDN&reg; is a registered trademark of Amateur Radio Emergency Data
      Network, Inc. The official AREDN repositories can be found at
      https://github.com/aredn.
