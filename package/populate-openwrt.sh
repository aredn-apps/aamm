#!/bin/bash

# This script runs from the top of the project directory and
# creates the filesystem image for the MeshChat API package.

IPK_DIR=$1

# Populate the CONTROL portion of the package
mkdir -p $IPK_DIR/CONTROL
cp -p package/openwrt/* $IPK_DIR/CONTROL/
sed -i "s%\$GITHUB_SERVER_URL%$GITHUB_SERVER_URL%" $IPK_DIR/CONTROL/control
sed -i "s%\$GITHUB_REPOSITORY%$GITHUB_REPOSITORY%" $IPK_DIR/CONTROL/control
sed -i "s%\$CI_SERVER_URL%$CI_SERVER_URL%" $IPK_DIR/CONTROL/control
sed -i "s%\$CI_PROJECT_PATH%$CI_PROJECT_PATH%" $IPK_DIR/CONTROL/control

# Populate the filesystem image for the package
install -d $IPK_DIR/www/aam
install -m 644 www/* $IPK_DIR/www/aam
install -d $IPK_DIR/www/cgi-bin
install -m 755 aamm.lua $IPK_DIR/www/cgi-bin
install -m 644 aamm_lib.lua $IPK_DIR/www/cgi-bin
install -m 644 aamm_config.lua $IPK_DIR/www/cgi-bin
install -d $IPK_DIR/usr/lib/lua/net
install -m 644 lib/json.lua $IPK_DIR/usr/lib/lua
install -m 644 lib/net/url.lua $IPK_DIR/usr/lib/lua/net
