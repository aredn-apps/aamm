#!/bin/bash

# This script runs from the top of the project directory and
# updates the version number in the control file for the package
# being built.

BUILD_DIR=$1

if [[ "${GITHUB_REF_TYPE}" == 'tag' ]]; then
    # ideally should only get version tags (i.e. 'v' followed by a number)
    if [[ "${GITHUB_REF_NAME}" =~ ^v[0-9].* ]]; then
        version="${GITHUB_REF_NAME#v}"
    fi
elif [[ -n "${CI_COMMIT_TAG}" ]]; then
    # ideally should only get version tags (i.e. 'v' followed by a number)
    if [[ "${CI_COMMIT_TAG}" =~ ^v[0-9].* ]]; then
        version="${CI_COMMIT_TAG#v}"
    fi
else
    # branch gets date code-branch_name-commit
    date=$(date +%Y%m%d)
    branch=$(git rev-parse --abbrev-ref HEAD)
    # maybe a detached head, so check common vars for branch name
    if [[ -n "${CI_COMMIT_REF_NAME}" ]]; then
        branch="${CI_COMMIT_REF_NAME}"
    elif [[ -n "${GITHUB_REF_NAME}" ]]; then
        branch="${GITHUB_REF_NAME}"
    fi
    commit=$(git rev-parse --short HEAD)
    version="${date}-${branch}-${commit}"
fi

# write the version to a VERSION file
echo "${version}" > VERSION
echo "Updating code references to version ${version}"

if [[ -f "${BUILD_DIR}/CONTROL/control" ]]; then
    control="${BUILD_DIR}/CONTROL/control"
elif [[ -f "${BUILD_DIR}/DEBIAN/control" ]]; then
    control="${BUILD_DIR}/DEBIAN/control"
fi
sed -i "s/^Version:.*/Version: $version/" "${control}"

# Update the version in aamm.lua if present
if [[ -f "${BUILD_DIR}/www/cgi-bin/aamm.lua" ]]; then
    sed -i "s/^APP_VERSION\s*=\s*.*$/APP_VERSION = \"${version}\"/" "${BUILD_DIR}/www/cgi-bin/aamm.lua"
fi
if [[ -f "${BUILD_DIR}/usr/lib/cgi-bin/aamm.lua" ]]; then
    sed -i "s/^APP_VERSION\s*=\s*.*$/APP_VERSION = \"${version}\"/" "${BUILD_DIR}/usr/lib/cgi-bin/aamm.lua"
fi
