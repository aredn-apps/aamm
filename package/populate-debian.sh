#!/bin/bash

# This script runs from the top of the project directory and
# creates the filesystem image for the MeshChat API package.

BUILD_DIR=$1

# Populate the CONTROL portion of the package
mkdir -p ${BUILD_DIR}/DEBIAN
cp -p package/debian/* ${BUILD_DIR}/DEBIAN
sed -i "s%\$GITHUB_SERVER_URL%$GITHUB_SERVER_URL%" ${BUILD_DIR}/DEBIAN/control
sed -i "s%\$GITHUB_REPOSITORY%$GITHUB_REPOSITORY%" ${BUILD_DIR}/DEBIAN/control
sed -i "s%\$CI_SERVER_URL%$CI_SERVER_URL%" ${BUILD_DIR}/DEBIAN/control
sed -i "s%\$CI_PROJECT_PATH%$CI_PROJECT_PATH%" ${BUILD_DIR}/DEBIAN/control

# Populate the filesystem image for the package
install -d ${BUILD_DIR}/var/www/html/aam
install -m 644 www/* ${BUILD_DIR}/var/www/html/aam

install -d ${BUILD_DIR}/usr/lib/cgi-bin
install -m 755 aamm.lua ${BUILD_DIR}/usr/lib/cgi-bin
install -m 644 aamm_lib.lua ${BUILD_DIR}/usr/lib/cgi-bin
install -m 644 aamm_config.lua ${BUILD_DIR}/usr/lib/cgi-bin

install -d ${BUILD_DIR}/usr/local/lib/lua/5.4/net
install -m 644 lib/json.lua ${BUILD_DIR}/usr/local/lib/lua/5.4
install -m 644 lib/net/url.lua ${BUILD_DIR}/usr/local/lib/lua/5.4/net

install -d ${BUILD_DIR}/usr/share/doc/aamm
install -m 444 CHANGELOG.md ${BUILD_DIR}/usr/share/doc/aamm
install -m 444 COPYING ${BUILD_DIR}/usr/share/doc/aamm/copyright
