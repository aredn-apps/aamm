
var config = {};
var current_aam_list = [];
var current_node_list = [];

$(function() {
    console.log("aamm starting");
    aamm_init();
});

function debug(msg) {
    if (config.debug) {
        console.log(msg);
    }
}

function make_id() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function quote(text) {
    var escapes = { " ": "#32;",
                    "\\+": "#43;", }

    Object.keys(escapes).forEach ((ch) => {
        text = text.replace(RegExp(ch, "g"), escapes[ch]);
    });

    return text;
}

function create_option(label, value) {
    value = value || label;
    return '<option value="' + value + '">' + label + '</option>'
}

function refresh_lists() {
    // populate the list of alerts
    aamm_api('list_alerts', (resp) => {
        add_to_file_list(resp["data"]);
    });

    // populate the list of nodes for quick selection
    aamm_api('list_nodes', (resp) => {
        add_to_node_list(resp["data"]);
    });
}

function add_to_file_list(files) {
    // Reset the current list of AAMs
    current_aam_list = [];
    $("#file-list").empty();

    files.forEach((entry) => {
        current_aam_list.push(entry["file"].replace(/\.txt$/, ""));
        $("#file-list").append(create_option(entry["file"].replace(/\.txt$/, ""), entry["file"]));
    });
}

function add_to_node_list(nodes) {
    current_node_list = [];
    $("#node-list").empty();

    // Add special selections
    $("#node-list").append(create_option("Select node name", "0"));
    $("#node-list").append(create_option("Other node not listed", "1"));
    $("#node-list").append(create_option("All nodes", "all"));

    // Add any group AAM names
    config["alert_groups"].forEach((group) => {
        if (jQuery.inArray(group.toLowerCase(), current_aam_list) == -1) {
            $("#node-list").append(create_option("Group: " + group, group));
        };
    });

    nodes.forEach((node) => {
        current_node_list.push(node);

        if (jQuery.inArray(node.toLowerCase(), current_aam_list) == -1) {
            $("#node-list").append(create_option(node));
        }
    })
}

function aamm_api(call) {
    var args = {"action": call};
    var fun = undefined;
    var url = config["api_url"] || '/cgi-bin/aamm.lua';

    if (arguments.length == 3) {
        args = Object.assign({}, args, arguments[1]);
        fun = arguments[2];
    } else {
        fun = arguments[1];
    }

    $.ajax({
        url: url,
        type: "GET",
        tryCount : 0,
        retryLimit : 3,
        cache: false,
        timeout: 5000,
        data: args,
        dataType: "json",
        context: this,
        success: function(data, textStatus, jqXHR)
        {
            if (data.status == 500) {
                console.error('Error receiving api results for ' + call + ': ' + data.response, 'red', {time: '30000'});
            } else {
                fun(data);
            }
        },
        // TODO Need to fix error handling
        error: function(jqXHR, textStatus, errorThrown)
        {
            if (textStatus == 'timeout') {
                this.tryCount++;
                if (this.tryCount <= this.retryLimit) {
                    //try again
                    $.ajax(this);
                    return;
                }
                //ohSnap('Error sending message: ' + textStatus, 'red', {time: '30000'});
            }
        }
    });
}

function set_edit_mode(mode) {
    debug("set_edit_mode(" + mode + ")");

    // type is create, update, off
    switch (mode) {
        case "create":
            $("#save-file").removeClass("hidden");
            $("#update-file").addClass("hidden");
            $("#cancel-change").removeClass("hidden");
            $("#delete-file").addClass("hidden");
            $("#alert-text").removeClass("hidden");

            $("#cancel-change").on("click", () => {
                set_edit_mode("off");
                $("#node-name").addClass("hidden");
                $("#node-name").val("");
                $("#node-list").removeClass("hidden");
                $("#node-list").val("0");
            });
            break;
        case "update":
            $("#save-file").addClass("hidden");
            $("#update-file").removeClass("hidden");
            $("#cancel-change").removeClass("hidden");
            $("#delete-file").removeClass("hidden");
            $("#alert-text").removeClass("hidden");

            $("#cancel-change").on("click", () => {
                set_edit_mode("off");
                $("#node-name").addClass("hidden");
                $("#node-name").val("");
                $("#node-list").removeClass("hidden");
                $("#node-list").val("0");
            });
            break;
        case "off":
            $("#save-file").addClass("hidden");
            $("#update-file").addClass("hidden");
            $("#cancel-change").addClass("hidden");
            $("#delete-file").addClass("hidden");
            $("#alert-text").addClass("hidden");

            // Turn off event handlers
            clear_edit_handlers();
            break;
    }
}

function clear_edit_handlers() {
    $("#delete-file").unbind();
    $("#cancel-change").unbind();
    $("#update-file").unbind();
    $("#save-file").unbind();
}

function handle_node_selection(element) {
    // TODO Unselect alert file name if selected from current alerts
    debug("handle_node_selection(" + element.val() + ")");
    clear_edit_handlers();

    if (element.val() == "0") {
        return
    };

    if (element.val() == "1") {
        $("#node-list").addClass("hidden");
        $("#node-name").removeClass("hidden");
    };

    $("#alert-text").val("");
    set_edit_mode("create");

    $("#save-file").on("click", () => {
        var alert_file;
        if (element.val() == "1") {
            alert_file = $("#node-name").val() + ".txt";
            $("#node-name").addClass("hidden");
        } else {
            alert_file = element.val() + ".txt";
        }

        aamm_api("create_alert", { "alert_file": alert_file,
                                   "alert_text": quote($("#alert-text").val()),
                                   "auth": config["password"] },
                (resp) => {
                    if (resp["result"] == "OK") {
                        debug(resp["data"]["result"]);

                        set_edit_mode("off");

                        // populate the list of alerts
                        aamm_api('list_alerts', (resp) => {
                            add_to_file_list(resp["data"]);
                        });

                        $("#node-list").removeClass("hidden");
                    } else {
                        console.error(resp["data"]["result"]);
                    }

                    refresh_lists();
        });
    });
}

function handle_file_selection(element) {
    debug("handle_file_selection(" + element.val() + ")");
    clear_edit_handlers();

    var alert_file = element.val();
    console.log("Alert " + alert_file + " selected");

    // get the alert text and put it in the textarea
    aamm_api('get_alert', { "alert_file": alert_file }, (resp) => {
        $("#alert-text").val(resp["data"]["message"]);
    });

    // configure editing area for updating
    set_edit_mode("update");
    $("#node-list").addClass("hidden");
    $("#node-name").addClass("hidden");

    $("#update-file").on("click", () => {
        aamm_api("update_alert", { "alert_file": alert_file,
                                   "auth": config["password"],
                                   "alert_text":  quote($("#alert-text").val()) },
            (resp) => {
                if (resp["result"] === "OK") {
                    debug(resp["data"]["result"]);
                } else {
                    console.error(resp["data"]["result"]);
                };
        });

        set_edit_mode("off");
        $("#node-list").removeClass("hidden");
    });

    $("#delete-file").on("click", () => {
        aamm_api("delete_alert", { "alert_file": alert_file,
                                   "auth": config["password"] },
            (resp) => {
                if (resp["result"] === "OK") {
                    debug(resp["data"]["result"]);
                } else {
                    console.error(resp["data"]["result"]);
                };
        });

        set_edit_mode("off");
        refresh_lists();
        $("#node-list").removeClass("hidden");
    });
}

function check_password() {
    aamm_api("check_credentials", { "auth": $("#password").val() }, (resp) => {
        if (resp['result'] === "OK") {
            debug("passwords match")
            config['password'] = $("#password").val();

            // Turn off info and password panel
            $("#info-panel").addClass("hidden");

            // Turn on basic editing
            $("#node-list").removeClass("hidden");
        } else {
            debug("passwords do not match");
        }
    });
}

function aamm_init() {
    $('#message').val('');
    aamm_id = Cookies.get('aamm_id');
    if (aamm_id == undefined) {
        Cookies.set('aamm_id', make_id(), {"SameSite": "Strict"});
        aamm_id = Cookies.get('aamm_id');
    }
    console.log("aamm ID = ", aamm_id);

    // retrieve the configured settings from the API
    aamm_api('settings', (resp) => {
        if (resp["result"] === "OK") {
            config = resp['data'];

            // If app_version looks like a semver version then prepend a 'v'
            if (config["app_version"].match(/^\d+\.\d+\.\d+$/))  {
                config["app_version"] = 'v' + config["app_version"];
                doc_version = config["app_version"];
            } else {
                doc_version = "main";
            }

            // Update the webpage with a few values
            $("#app_version").text(config["app_version"]);
            var doc_ref = $("#doc-ref").attr("href").replace("VERSION", doc_version);
            $("#doc-ref").attr("href", doc_ref);
        } else {
            console.error("Failure getting settings");
        }
    });

    refresh_lists();

    $('#node-list').on("change", () => { handle_node_selection($('#node-list')) });
    $('#file-list').on("change", () => { handle_file_selection($('#file-list')) });

    // Allow user to authenticate
    $("#login").on("click", () => {
        debug("login button pressed");

        check_password();
    });

    $('#password').keypress((event) => {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            check_password();
        }
      });
}
