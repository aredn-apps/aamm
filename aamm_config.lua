
Config = {
    aam_dir = '/www/aam',
    api_url = '/cgi-bin/aamm.lua',
    password = 'change_me',

    debug = false,
    lock_file = '/www/aam/lock',
    alert_groups = {  },
}

return Config
